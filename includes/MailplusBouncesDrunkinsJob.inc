<?php
/**
 * @file
 * Contains MailplusBouncesDrunkinsJob.
 */

use Mailplus\MailplusMarcomRest;

/**
 * Drunkins Job class to deactivate MailPlus contacts which have hard bounces.
 *
 * 'deactivating' means resetting one specific permissions bit.
 *
 * There's a possible 'race condition' as with many jobs: whether a permission
 * should be disabled is determined on start(), and disabling it happens on
 * processItem(). If the contact is updated in Mailplus in the meantime from
 * another source, it will still be disabled. (Other changed details won't be
 * overwritten.)
 *
 * This class doesn't inject all settings/functionality; it calls global
 * functions mailplus_init_client() and drupal_mail().
 */
class MailplusBouncesDrunkinsJob extends DrunkinsJob {
  /**
   * Settings used by this job:
   *
   * - mailplus_permission_property (string; default 'permissions'):
   *   Name of the Mailplus property/field holding the 'permissions' bit.
   * - mailplus_permission_bitmask:
   *   Bitmask of the permission(s) to turn off. E.g. to turn off bit 3 in this
   *   field, specify '8'. (This is equal to the 'bit' value that would be
   *   returned in a contact's data from a Mailplus REST API call.)
   * - mailplus_bounces_mailto (string):
   *   If nonempty, it's assumed to be an e-mail address and mail will be sent
   *   to this address about the deactivated contacts. See finish() for details.
   */

  /**
   * Constructor function. Sets up the necessary settings.
   */
  public function __construct(array $settings = array()) {

    // Set 'drunkins_fetcher_class' if not done yet. (Note this is usually not
    // configured in hook_queue_info as a setting, but it is used by the parent
    // class as a setting - and drunkins.module takes care of copying it into
    // settings.)
    if (!isset($settings['drunkins_fetcher_class'])) {
      $settings['drunkins_fetcher_class'] = 'MailplusDrunkinsFetcher';
    }
    if (!isset($settings['mailplus_fetch_type'])
        && $settings['drunkins_fetcher_class'] === 'MailplusDrunkinsFetcher') {
      $settings['mailplus_fetch_type'] = 'bounces';
    }

    if (!isset($settings['mailplus_permission_property'])) {
      $settings['mailplus_permission_property'] = 'permissions';
    }

    parent::__construct($settings);
  }

  /**
   * Check settings; log anything not OK. (The method arguments are just in
   * case this grows into a big list of checks later.)
   *
   * @param array $context
   *   Context as passed to start() / processItem().
   * @param bool $quiet_fast
   *   If TRUE, do not log, and exit as soon as something not OK is found.
   *   It can be assumed that whenever this method is called with $quiet_fast
   *   TRUE, it has already been called without this argument for the same
   *   process, and there is no need to do a full accounting anymore.
   *
   * @return bool
   *   TRUE if all OK.
   */
  protected function checkSettings(array $context, $quiet_fast = FALSE) {
    $ok = TRUE;

    if (empty($this->settings['mailplus_permission_bitmask'])) {
      // 'shorthand' to set $ok to FALSE when it matters; makes sense when there
      // are lots of checks in this method:
      if ($ok = $quiet_fast) {
        return FALSE;
      }
      $this->log('@setting setting not configured or zero!', array('@setting' => 'mailplus_permission_bitmask'), WATCHDOG_ERROR);
    }

    if ($quiet_fast) {
      return TRUE;
    }

    // Optional checks that don't really make sense on every processItem():
    if (!empty($this->settings['mailplus_bounces_mailto'])
        && !filter_var($this->settings['mailplus_bounces_mailto'], FILTER_VALIDATE_EMAIL)) {
      $ok = FALSE;
      $this->log('@setting setting has invalid value @value!', array('@setting' => 'mailplus_bounces_mailto', '@value' => $this->settings['mailplus_bounces_mailto']), WATCHDOG_ERROR);
    }

    return $ok;
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm() {
    $form = parent::settingsForm();

    $form['mailplus_bounces_mailto'] = array(
      '#type' => 'textfield',
      '#title' => t('E-mail details to:'),
      '#description' => t('Optional e-mail address to send a summary of countacts with hard bounces, which this job will disable.'),
    );

    // Since this form is the one interactive thing in the process, it's also a
    // good place for global checks that should generate user errors.
    $this->checkSettings(array());

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function start(array &$context) {

    $context += array(
      'disabled' => 0,
      'error' => 0,
      'skipped' => 0,
    );

    // This is the last place where we can do checks on strangenesses that will
    // make processItem() not do anything.
    // (Better give errors once here than <items> times later.)
    if (!$this->checkSettings($context)) {
      return array();
    }

    // We assume the parent (i.e. a fetcher) takes care of fetching items.
    // (Abstracting this enables us to use a rolling timestamp.)
    $bounces = parent::start($context);

    $perm_property = $this->settings['mailplus_permission_property'];
    $perm_mask = $this->settings['mailplus_permission_bitmask'];

    $items = array();
    foreach ($bounces as $i => $bounce) {
      if (isset($bounce['type']) && $bounce['type'] === 'HARDBOUNCE') {

        if (empty($bounce['contact']) || !is_array($bounce['contact'])) {
          $this->log("Bounce #@index has no '@property' value of type '@type'.", array('@index' => $i, '@property' => 'contact', '@type' => 'array'), WATCHDOG_ERROR);
          $context['error']++;
          continue;
        }
        $contact = $bounce['contact'];
        // $i shall only be used in log messages.
        if (isset($contact['externalId'])) {
          $i .= " (externalID $contact[externalId])";
        }
        // I guess for hard bounces, 'temporary' is never true. If it is, skip
        // silently.
        if (!empty($contact['temporary']) || !empty($contact['testGroup'])) {
          continue;
        }

        // This can't happen but check anyway.
        if (!isset($contact['externalId']) && !isset($contact['properties']['email'])) {
          $this->log("Contact for bounce #@index has no '@property' property.", array('@index' => $i, '@property' => 'externalId or email', '@value' => $bounce['type']), WATCHDOG_ERROR);
          $context['error']++;
          continue;
        }

        if (empty($bounce['date']) || !is_numeric($bounce['date'])) {
          $this->log("Bounce #@index has no '@property' value of type '@type'.", array('@index' => $i, '@property' => 'date', '@type' => 'numeric'), WATCHDOG_ERROR);
          $context['error']++;
          continue;
        }
        if (empty($contact['lastChanged']) || !is_numeric($contact['lastChanged'])) {
          $this->log("Contact for bounce #@index has no '@property' value of type '@type'.", array('@index' => $i, '@property' => 'lastChanged', '@type' => 'numeric'), WATCHDOG_ERROR);
          $context['error']++;
          continue;
        }
        // If contact has changed after having bounced, we do not process it;
        // the e-mail address may not be the one that bounced anymore. Log this.
        if ($contact['lastChanged'] > $bounce['date']) {
          $this->log("Bounce #@index has been updated after bounce date @date; skipping.", array('@index' => $i, '@date' => date('c', (int)substr($bounce['date'], 0, 10))), WATCHDOG_INFO);
          continue;
        }

        if (empty($contact['properties'][$perm_property])) {
          $this->log("Contact for bounce #@index has no '@property' property.", array('@index' => $i, '@property' => "$perm_property"), WATCHDOG_ERROR);
          $context['error']++;
          continue;
        }

        // Gather data for processing hard bounce.

        $details = array(
          'id' => isset($contact['externalId']) ? $contact['externalId'] : '',
          'email' => isset($contact['properties']['email']) ? $contact['properties']['email'] : '',
          'organisation' => isset($contact['properties']['organisation']) ? $contact['properties']['organisation'] : '',
          'firstName' => isset($contact['properties']['firstName']) ? $contact['properties']['firstName'] : '',
          'lastName' => isset($contact['properties']['lastName']) ? $contact['properties']['lastName'] : '',
          'created' => date('c', (int) substr($contact['created'], 0, 10)),
          'lastChanged' => date('c', (int) substr($contact['lastChanged'], 0, 10)),
          'Bounce date' => date('c', (int) substr($bounce['date'], 0, 10)),
        );
        if (!empty($this->settings['list_format'])) {
          // Return some details for display.
          $items[] = $details;
        }
        else {
          // Only return externalId plus the data that needs updating. If we
          // don't have externalId, we use the email as a contact's 'id'; in the
          // context of 'handling bounces' this is not ambiguous. (We still
          // prefer externalId because updating a contact by email will also
          // update only one record even if the email matches multiple - see
          // comments at MailplusMarcomRest::createContact().)
          if (isset($contact['externalId'])) {
            $item = array('externalId' => $contact['externalId']);
          }
          else {
            $item = array('properties' => array('email' => $contact['properties']['email']));
          }

          // Reset the permission bit(s).
          $reset = FALSE;
          foreach ($contact['properties'][$perm_property] as $perm) {
            if (!empty($perm['enabled'])
                && isset($perm['bit'])
                && $perm_mask & $perm['bit']) {
              // This bit should be reset.
              if (!isset($item['properties'][$perm_property])) {
                $item['properties'][$perm_property] = array();
              }
              $item['properties'][$perm_property][] = array(
                'bit' => $perm['bit'],
                'enabled' => FALSE,
              );
              $reset = TRUE;
            }
          }
          if (!$reset) {
            $this->log("Bounce #@index does not have any of the permissions in 'reset bitmask' (@value) set, so won't be updated.", array(
              '@index' => $i . (isset($contact['properties']['email']) ? ' (' . $contact['properties']['email'] . ')' : ''),
              '@value' => $perm_mask), WATCHDOG_NOTICE);
            $context['skipped']++;
            continue;
          }

          // Set details in $context, to use in the mail on finish(). (All
          // details - even if some of them fail during processItem(), the mail
          // will include them by default.)
          $id = !empty($details['id']) ? $details['id'] : $details['email'];
          $context['mailplus_bounces'][$id] = $details;

          $items[] = $item;
        }

      }

      // Don't process SOFTBOUNCE, but log an error for other types - they
      // should never happen.
      elseif (!isset($bounce['type'])) {
        $this->log("Bounce #@index has no '@property' property.", array(
          '@index' => $i . (isset($bounce['contact']['properties']['email']) ? ' (' . $bounce['contact']['properties']['email'] . ')' : ''),
          '@property' => 'type'), WATCHDOG_CRITICAL);
        $context['error']++;
      }
      elseif ($bounce['type'] !== 'SOFTBOUNCE') {
        $this->log("Bounce #@index has unrecognized '@property' property value: @value.", array(
          '@index' => $i . (isset($bounce['contact']['properties']['email']) ? ' (' . $bounce['contact']['properties']['email'] . ')' : ''),
          '@property' => 'type', '@value' => $bounce['type']), WATCHDOG_CRITICAL);
        $context['error']++;
      }
      // Skip softbounces silently, without increasing 'skipped'.
    }

    // The caller will report count($items) items being returned. The
    // 'skipped' counter will / 'error' counter may include items not among that
    // count, in finish().
    return $items;
  }

  /**
   * {@inheritdoc}
   */
  public function processItem($item, array &$context) {

    if (!$this->checkSettings($context, TRUE)) {
      // We can't do anything. We've warned in start(), so exit silently.
      return;
    }

    //@todo this class has not been tested in a Mailplus environment with
    // multiple permission bits, only one of which is reset here (which means
    // the other one is not present in $item['properties']['permissions']).
    // Supposedly, the bit which is not in $item, remains untouched instead of
    // being deleted/reset.
    // We trust the Mailplus REST API which is well thought out, but:
    // explicitly test this sometime.

    mailplus_init_client();
    // We can't call updateContact() if we don't have an externalId.
    // We could always call createContact() but it's ever so slightly more
    // ambiguous so we prefer updateContact() if we can.
    $response = isset($item['externalId'])
      ? MailplusMarcomRest::updateContact($item)
      : MailplusMarcomRest::createContact($item, array('update' => TRUE));

    if (isset($response['errorType'])) {
      $this->log('Update of item @item failed', array('@item' => $item['externalId']), WATCHDOG_ERROR);
      $context['error']++;
    }
    else {
      $context['disabled']++;
    }
  }

  /**
   * {@inheritdoc}
   */
  public function finish(array &$context) {

    // NOTE: skipped/error counts may include items not in the 'number of items'
    // that were returned from start()! If that's confusing, rewrite accounting.

    // We're not logging the contact IDs/e-mail addresses themselves here.
    // Change $context['disabled'] to an array if you want to.
    $message = format_plural($context['disabled'], 'Disabled 1 bounced contact', 'Disabled @count bounced contacts');
    if ($context['skipped']) {
      $message .= ", $context[skipped] skipped for various reasons";
    }
    if ($context['error']) {
      $message .= '; ' . format_plural($context['error'], '1 error encountered', '@count errors encountered');
    }

    if ($context['disabled']
        && !empty($this->settings['mailplus_bounces_mailto'])
        // Don't log invalid e-mail; we already did.
        && filter_var($this->settings['mailplus_bounces_mailto'], FILTER_VALIDATE_EMAIL)) {

      // We use mailplus for the mail definition, because we don't want to
      // depend on outside modules being disabled. If you want to change the
      // message, use hook_mail_alter().
      drupal_mail('mailplus', 'mailplus_disabled_bounces', $this->settings['mailplus_bounces_mailto'], language_default(), array(
        'disabled' => $context['disabled'],
        'errors' => $context['error'],
        'details' => $context['mailplus_bounces'],
        'job_id' => $this->settings['job_id'],
      ));
    }

    return $message . '.';
  }

}
