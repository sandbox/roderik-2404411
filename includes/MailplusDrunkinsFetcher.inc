<?php
/**
 * @file
 * Contains MailplusDrunkinsFetcher.
 */

use Mailplus\MailplusMarcomRest;

/**
 * Drunkins Fetcher class for Mailplus.
 *
 * This class exists for Drunkins jobs that want to use standard DrunkinsFetcher
 * functionality, like a 'rolling timestamp' for periodic jobs, limiting or
 * caching items. (See comments in DrunkinsFetcher for the respective settings
 * to enable that functionality.)
 *
 * In exchange for getting that functionality, this class needs to implement
 * fetchItems(), using a 'mailplus_fetch_type' setting to enable generic use:
 * see the code.
 *
 * This class doesn't inject all settings/functionality; it calls global
 * (Drupal) function mailplus_init_client().
 */
class MailplusDrunkinsFetcher extends DrunkinsFetcher {

  /**
   * Constructor function. Sets up the necessary settings.
   */
  public function __construct(array $settings = array()) {
    // We need a running timestamp (or a 'fetcher_timestamp_from_date' setting)
    // in fetchItems(), which is enabled with the following.
    $settings['opt_fetcher_timestamp'] = TRUE;
    parent::__construct($settings);
  }

  /**
   * {@inheritdoc}
   */
  protected function fetchItems(array &$context) {
    if (!isset($context['fetcher_timestamp_from'])) {
      return 'fetcher_timestamp_from not set in context!';
    }
    if (!is_numeric($context['fetcher_timestamp_from'])) {
      return 'fetcher_timestamp_from in context is not numeric!';
    }

    mailplus_init_client();

    // Dates:
    // 'to date' is not supported yet.
    // 'from date' is 'fetcher_timestamp_from' context value (which the parent
    //   retrieves from the 'fetcher_timestamp_from_date' setting (a
    //   PHP-parseable date expression, or if that is not set, from an
    //   automatically-updated timestamp set in a Drupal variable.
    //
    // Type of data:
    // 'mailplus_fetch_type' (optional) setting can have the values given below.
    switch (isset($this->settings['mailplus_fetch_type']) ?
            $this->settings['mailplus_fetch_type'] : 'updates') {
      case 'updates':
        return MailplusMarcomRest::getContactUpdates($context['fetcher_timestamp_from']);
      case 'bounces':
        return MailplusMarcomRest::getContactBounces($context['fetcher_timestamp_from']);
    }

    return 'Unknown mailplus_fetch_type setting!';
  }

}
