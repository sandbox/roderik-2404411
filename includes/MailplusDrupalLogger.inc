<?php
/**
 * Contains MailplusDrupalLogger.
 */

use Mailplus\MailplusLoggerException;
use Mailplus\MailplusNullLogger;

/**
 * Class MailplusDrupalLogger
 *
 * Implements PSR-3 LoggerInterface. (Though not officially, because the
 * interface file is not included in Drupal sources.)
 *
 * Features:
 * - Various log methods can be enabled (via 'config options' passed at class
 *   instantiation): drupal_set_message(), watchdog(), and/or throwing an
 *   exception.
 * - Log type (string) can be set for use by some methods.
 * - Per-method config options for verbosity / extra information added to log
 *   messages, which is passed in the log context. This is tuned to API calls.
 * - Some (by definition imperfect because we can't know all callers' behavior)
 *   effort is taken to not log duplicate information, when the 'verbosity
 *   options' overlap debug-level logging. This is tuned to MailplusRestClient
 *   behavior. See comments on 'mailplus_extra_info'.
 */
class MailplusDrupalLogger extends MailplusNullLogger {

  // First, our own extensions which are not part of LoggerInterface

  /**
   * Add no extra context info to log messages.
   */
  const ADD_LOG_INFO_NONE = 0;

  /**
   * Add specific 'call_info' context (useful for API calls) to log messages.
   */
  const ADD_LOG_INFO_CALL = 1;

  /**
   * Add all context info to log messages.
   */
  const ADD_LOG_INFO_ALL = 2;

  /**
   * Map of PSR-3 logging constants to drupal_set_message() supported constants.
   */
  protected static $messageLevelMap = array(
    'emergency' => 'error',
    'alert' => 'error',
    'critical' => 'error',
    'error' => 'error',
    'warning' => 'warning',
    // Mapping of 'notice' is disputable. Since we will use this class to print
    // more warnings than errors, a notice being 'status' will make it stand out
    // more in between most other messages.
    'notice' => 'status',
    'info' => 'status',
    'debug' => 'status',
  );

  /**
   * A a string (usually a machine name) to be used for the log type
   * for actions which support/need it. (At the moment this is watchdog.)
   *
   * This is decoupled from the 'config' variable because it's typically set
   * by code using this class, whereas 'config' is determined by the user/
   * website configuration.
   * It doesn't use a setter/getter method, so it won't make switching between
   * PSR-3 compatible classes more difficult.
   *
   * @var string
   */
  public $logType;

  /**
   * Constructor function.
   * 
   * @param array $config
   *   (optional) Configuration options for this class. The following keys are
   *   'methods' and values are 'log levels'; if events equal to / stronger
   *   than this level are logged, the message is logged using the corresponding
   *   method. If values are not a known (lower cased) log level, the methods
   *   are ignored.
   *   Valid actions are:
   *   - watchdog:  log to watchdog
   *   - message:   set message using drupal_set_message().
   *   - exception: throw exception.
   *   The following keys have integer values to indicate whether extra verbose
   *   information should be logged (see class constants for values):
   *   - watchdog_extra
   *   - message_extra
   */
  function __construct(array $config = array('watchdog' => 'debug')) {
    $this->config = $config;
    // Set default log type.
    $this->logType = 'mailplus';
  }

  /**
   * Logs a message using various methods, including extra information,
   * depending on class configuration. Some of the extra information is tailored
   * to API calls.
   *
   * @param mixed $level
   * @param string $message
   * @param array $context
   *
   * @return null
   *
   * @throws MailplusLoggerException
   */
  public function log($level, $message, array $context = array()) {

    // If $level is unknown, we will fall through to the parent for throwing
    // an exception as required by PSR-3.
    if (isset(static::$levelMap[$level])) {

      $log_watchdog = isset($this->config['watchdog'])
        && isset(static::$levelMap[$this->config['watchdog']])
        && static::$levelMap[$level] <= static::$levelMap[$this->config['watchdog']];
      $log_message = isset($this->config['message'])
        && isset(static::$levelMap[$this->config['message']])
        && static::$levelMap[$level] <= static::$levelMap[$this->config['message']];

      if ($log_watchdog || $log_message) {

        $extra_info_level_watchdog = isset($this->config['watchdog_extra'])
          ? $this->config['watchdog_extra'] : self::ADD_LOG_INFO_NONE;
        $extra_info_level_message = isset($this->config['message_extra'])
          ? $this->config['message_extra'] : self::ADD_LOG_INFO_NONE;

        // Assumption about the code calling this class: if the placeholders
        // contain 'mailplus_extra_info', the caller is explicitly logging extra
        // (debug) info to go with a message it's logged just before.
        // (Reason: that code can't assume it's working with this logger, so
        // cannot assume the extra info is actually added to an error call.)
        // We already output that info with the first call if our *_extra config
        // says so, in which case we can ignore the extra debug-log.
        if (!empty($context['mailplus_extra_info'])) {
          $log_watchdog = $log_watchdog && $extra_info_level_watchdog != self::ADD_LOG_INFO_ALL;
          $log_message = $log_message && $extra_info_level_message != self::ADD_LOG_INFO_ALL;
        }
      }

      if ($log_watchdog || $log_message) {

        // Convert PSR-3 placeholders to Drupal placeholders.
        // Also set aside non-placeholder context which we don't handle explicitly,
        // so we can log it. (This includes 'exception'.)
        $placeholders = array();
        $more_context = array();
        foreach ($context as $key => $value) {
          if (strpos($message, '{' . $key . '}') !== FALSE) {
            $message = str_replace('{' . $key . '}', "@$key", $message);
            $placeholders["@$key"] = $value;
          }
          else {
            // Compile array of all non-placeholder context.
            $more_context[$key] = $value;
          }
        }

        // Log to watchdog if configured.
        if ($log_watchdog) {
          $msg = $message;
          // Put error plus optional extra details in one log message.
          $extra_msg = $this->mailplusLoggerExtraInfo($extra_info_level_watchdog, $level, $more_context, $placeholders);
          if ($extra_msg) {
            $msg .= '<br>' . $extra_msg;
          }
          // levelMap values happen to coincide with watchdog severity constants...
          watchdog($this->logType, $msg, $placeholders, static::$levelMap[$level]);
        }

        // Set message on screen if configured.
        if ($log_message) {
          $msg = $message;
          if ($extra_info_level_watchdog > $extra_info_level_message) {
            $msg .= '<br/>See Recent Log Messages for more details.';
          }
          drupal_set_message(t($msg, $placeholders), static::$messageLevelMap[$level]);

          // Print call details / extra info as separate message
          $msg = $this->mailplusLoggerExtraInfo($extra_info_level_message, $level, $more_context, $placeholders);
          if ($msg) {
            drupal_set_message(t("Details for above:<br>$msg", $placeholders), static::$messageLevelMap[$level]);
          }
        }

      }
    }

    // Throw exception if required (by wrong $level) or configured.
    // We do this after logging other methods, on purpose.
    return parent::log($level, $message, $context);
  }

  /**
   * Returns HTML string containing 'extra info', derived from context in some
   * placeholders. to be added to an initial message by the caller. Adds
   * placeholders where necessary.
   * (Since the extra message is dynamic and the caller uses t() on it, this can
   * put many different similar messages inside the translation system - but if
   * we'd move everything into a giant placeholder, we couldn't use <br>s in
   * between all those different details.)
   *
   * @param int $extra_info_level
   *   Level of logging extra info as set for this class. (Higher is more info.)
   * @param int $level
   *   Message severity level.
   * @param array $context
   *   Context, without the placeholders for the initial message.
   * @param array $placeholders
   *   Placeholders for initial message; passed by reference; extra placeholders
   *   are added. Note they are re-added upon consecutive calls; this does no
   *   damage.
   *
   * @return string
   *   Extra info (which can be added to the initial message).
   */
  private function mailplusLoggerExtraInfo($extra_info_level, $level, array $context, array &$placeholders) {

    // Make sure we can just refer to $call_info by any key without notices:
    $call_info =
      (isset($context['call_info']) && is_array($context['call_info'])
        ? $context['call_info'] : array())
      + array(
        'errorType' => '',
        'url' => '',
        'method' => '',
        'parameters' => '',
        'time' => ''
      );

    $extra_messages = array();

    // See comments in log() for 'mailplus_extra_info'.
    // This logger wants to output the 'standard call info', either with any
    // log message (if it's configured to do so) or with the second
    // 'mailplus_extra_info' messsage but not with both; that's redundant.
    if (empty($placeholders['@mailplus_extra_info']) && $extra_info_level != self::ADD_LOG_INFO_NONE
        || !empty($placeholders['@mailplus_extra_info']) && $extra_info_level == self::ADD_LOG_INFO_NONE) {

      // Add method/url/time/parameters from 'call_info' context.
      if ($call_info['url']) {
        $extra_messages[] = 'Method/URL: @mailplus_method @mailplus_url';
        $placeholders['@mailplus_method'] = $call_info['method'] ;
        $placeholders['@mailplus_url'] = $call_info['url'];
        unset($context['call_info']['method']);
        unset($context['call_info']['url']);
      }
      if (isset($call_info['time']) && is_numeric($call_info['time'])) {
        // This is equivalent to date('r') except it does not emit warnings if
        // the current timezone is not set in php.ini.
        $extra_messages[] = 'Start time: @mailplus_time';
        $placeholders['@mailplus_time'] = format_date($call_info['time'], 'long');
        unset($context['call_info']['time']);
      }
      if ($call_info['parameters']) {
        // Enclose var-dump stuff in <pre>; we want to preserve the newlines
        // (and can't replace those by <br> because they would be escaped), plus
        // it's better formatted in a monospace font.
        $extra_messages[] = 'Parameters:<br><pre>@mailplus_params</pre>';
        $placeholders['@mailplus_params'] = $this->varDump($call_info['parameters']);
        unset($context['call_info']['parameters']);
      }
    }
    if (!$context['call_info']) {
      unset($context['call_info']);
    }

    // Add any extra context info if we're configured to do this.
    if ($extra_info_level == self::ADD_LOG_INFO_ALL) {

      // 1. All other data from context (may include exception, and all pieces
      //    of call_info that were not output above).
      foreach ($context as $key => $value) {
        // Turn non-placeholder info into
        if (is_object($value)) {
          $context[$key] = method_exists($value, '__toString') ? (string)$value : $this->varDump($value);
        }
        elseif (is_scalar($value)) {
          $context[$key] = (string)$value;
        }
        else {
          $context[$key] = $this->varDump($value);
        }
        $extra_messages[] = 'More context:<br><pre>@mailplus_context</pre>';
        $placeholders['@mailplus_context'] = $this->varDump($context);
      }

      // 2. Backtrace. We assume we don't need to do this if an exception is
      //    present because its __toString() method will have already output it.
      //    (Change this code if that assumption turns out untrue.)
      //    Only do this for error and higher; this is non-configurable.
      if ($level <= WATCHDOG_ERROR) {
        $backtrace = array();
        foreach (debug_backtrace(DEBUG_BACKTRACE_IGNORE_ARGS) as $k => $v) {
          // Ignore first lines. We won't print the call to log() and this
          // function.
          if ($k > 1) {
            $backtrace[] = '#' . ($k - 1)
                           . (isset($v['file']) ? " $v[file]($v[line]):" : '')
                           . (isset($v['class']) ? $v['class'] . '->' : '')
                           . " $v[function]()";
          }
        }
        $extra_messages[] = 'Backtrace:<br><pre>@backtrace</pre>';
        $placeholders['@backtrace'] = implode("\n", $backtrace);
      }
    }

    return implode('<br>', $extra_messages);
  }

  /**
   * Return a string representation of a variable.
   */
  protected function varDump(&$var) {
    if (is_array($var)) {
      // print_r is better on the eye than var_dump. However, it prints
      // 1/'' for TRUE/FALSE, so fix that. Unlike var_dump, the output is
      // ambiguous but we don't mind that for log output.
      array_walk_recursive($var, function (&$value) {
        if (is_bool($value)) {
          $value = $value ? 'True' : 'False';
        }
      });
    }
    return print_r($var, TRUE);
  }

}
