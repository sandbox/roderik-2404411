<?php
/**
 * @file
 * Admin screens for Mailplus API.
 */

use Mailplus\MailplusMarcomRest;

/**
 * Form definition for global configuration.
 */
function mailplus_config_form($form, &$form_state) {

//@todo look at either CTools or Variable.module as a way of creating different sets of these variables.
//   Bonus points for keeping the config forms alterable. (Variable realms instead of Ctools export configs?)
//@todo hook_mailplus_client_info plus choice here.

  $form['help'] = array(
    '#markup' => t('Below settings are used by code which uses the simplest mode of initializing a MailPlus connection: calling mailplus_init_client().'),
    '#prefix' => '<div class="help">',
    '#suffix' => '</div>',
  );

  $form['mailplus_api_key'] = array(
    '#type' => 'textfield',
    '#title' => t('Mailplus REST API Key'),
    '#default_value' => variable_get('mailplus_api_key', ''),
  );
  $form['mailplus_api_secret'] = array(
    '#type' => 'textfield',
    '#title' => t('Mailplus REST API Secret'),
    '#default_value' => variable_get('mailplus_api_secret', ''),
  );

  $form['logger'] = array(
    '#type' => 'fieldset',
    '#title' => t('Logger configuration'),
    '#description' => t('These settings may not work for code which uses its own logger.'),
  );
  // These are various combinations of the 'watchdog' + 'watchdog_extra' options
  // for the logger class.
  $form['logger']['mailplus_log_watchdog'] = array(
    '#type' => 'select',
    '#title' => t('Log MailPlus errors to watchdog'),
    '#options' => array(
      0 => t('No'),
      1 => t('Log errors'),
      2 => t('Log errors including call/parameters'),
      3 => t('Log all messages including all context info + backtrace'),
    ),
    '#default_value' => variable_get('mailplus_log_watchdog', 1),
  );
  $form['logger']['mailplus_log_screen'] = array(
    '#type' => 'select',
    '#title' => t('Show MailPlus errors on screen'),
    '#options' => array(
      0 => t('No'),
      1 => t('Log errors'),
      2 => t('Log errors including call/parameters'),
      3 => t('Log all messages including all context info + backtrace'),
    ),
    '#default_value' => variable_get('mailplus_log_screen', 0),
  );

  return system_settings_form($form);
}

/**
 * Form definition for some test functions.
 */
function mailplus_test_form($form, &$form_state) {

  $form['help'] = array(
    '#markup' => t('On this form you can perform some REST API calls, which eases testing API access and accessing certain data.'),
    '#weight' => 0,
  );

  // Put each function in its own fieldset and add needed widgets one by one.
  // (It's a bit repetitive, but not so much it needs refactoring.)

  $form['get_contact'] = array(
    '#type' => 'fieldset',
    '#title' => t('Get Contact'),
    '#weight' => 1,
  );
  $form['get_contact']['get_contact_id'] = array(
    '#type' => 'textfield',
    '#title' => t('External ID'),
    '#description' => t('A unique ID according to a scheme which you, as the external system, can decide yourself.'),
  );
  $form['get_contact']['get_contact_submit'] = array(
    '#type' => 'submit',
    '#value' => t('Get Contact'),
  );

  $form['get_contact_updates'] = array(
    '#type' => 'fieldset',
    '#title' => t('Get Contact Updates'),
    '#weight' => 2,
  );
  $form['get_contact_updates']['get_contact_updates_start'] = array(
    '#type' => 'textfield',
    '#title' => t('Start date'),
    '#description' => t('Any format supported by date_create()'),
  );
  $form['get_contact_updates']['get_contact_updates_end'] = array(
    '#type' => 'textfield',
    '#title' => t('End date.'),
    '#description' => t("Defaults to 'now'."),
  );
  $form['get_contact_updates']['get_contact_updates_submit'] = array(
    '#type' => 'submit',
    '#value' => t('Get Contact Updates'),
  );
  $form['get_contact_updates']['help'] = array(
    '#type' => 'markup',
    '#markup' => t("Please note this is not a full list of contacts; contacts which have been created once and never changed afterwards, don't appear in this list."),
    '#prefix' => '<div class="description">',
    '#suffix' => '</div>'
  );

  $form['get_contact_bounces'] = array(
    '#type' => 'fieldset',
    '#title' => t('Get Contact Bounces'),
    '#weight' => 3,
  );
  $form['get_contact_bounces']['get_contact_bounces_start'] = array(
    '#type' => 'textfield',
    '#title' => t('Start date'),
    '#description' => t('Any format supported by date_create()'),
  );
  $form['get_contact_bounces']['get_contact_bounces_end'] = array(
    '#type' => 'textfield',
    '#title' => t('End date.'),
    '#description' => t("Defaults to 'now'."),
  );
  $form['get_contact_bounces']['get_contact_bounces_submit'] = array(
    '#type' => 'submit',
    '#value' => t('Get Contact Bounces'),
  );

  $form['get_properties'] = array(
    '#type' => 'fieldset',
    '#title' => t('Get Contact Properties'),
    '#weight' => 4,
  );
  $form['get_properties']['get_properties_submit'] = array(
    '#type' => 'submit',
    '#value' => t('Get Contact Properties'),
  );

  $form['get_campaigns'] = array(
    '#type' => 'fieldset',
    '#title' => t('Get Campaigns'),
    '#weight' => 5,
  );
  $form['get_campaigns']['get_campaigns_submit'] = array(
    '#type' => 'submit',
    '#value' => t('Get Campaigns'),
  );

  return $form;
}

/**
 * Validate handler for test form.
 */
function mailplus_test_form_validate($form, &$form_state) {

  switch ($form_state['clicked_button']['#value']) {
    case t('Get Contact'):
      if (empty($form_state['values']['get_contact_id'])) {
        form_error($form['get_contact']['get_contact_id'],
          t('!name field is required.' . ' ' . t("(Enter 'all' for all contacts.)"), array('!name' => $form['get_contact']['get_contact_id']['#title'])));
      }
      elseif (!is_numeric($form_state['values']['get_contact_id'])
              && $form_state['values']['get_contact_id'] !== 'all') {
        form_error($form['get_contact']['get_contact_id'],
          t('!name field must be numeric or @value.', array('!name' => $form['get_contact']['get_contact_i1']['#title'], '@value' => "'all'")));
      }
      break;

    case t('Get Contact Updates'):
      if (empty($form_state['values']['get_contact_updates_start'])) {
        form_error($form['get_contact_updates']['get_contact_updates_start'],
          t('!name field is required.', array('!name' => $form['get_contact_updates']['get_contact_updates_start']['#title'])));
      }
      elseif (!date_create($form_state['values']['get_contact_updates_start'])) {
        form_error($form['get_contact_updates']['get_contact_updates_start'],
          t('Invalid date format given for !name.', array('!name' => $form['get_contact_updates']['get_contact_updates_start']['#title'])));
      }
      if (!empty($form_state['values']['get_contact_updates_end'])
          && !date_create($form_state['values']['get_contact_updates_end'])) {
        form_error($form['get_contact_updates']['get_contact_updates_end'],
          t('Invalid date format given for !name.', array('!name' => $form['get_contact_updates']['get_contact_updates_end']['#title'])));
      }
      break;

    case t('Get Contact Bounces'):
      if (empty($form_state['values']['get_contact_bounces_start'])) {
        form_error($form['get_contact_bounces']['get_contact_bounces_start'],
          t('!name field is required.', array('!name' => $form['get_contact_bounces']['get_contact_bounces_start']['#title'])));
      }
      elseif (!date_create($form_state['values']['get_contact_bounces_start'])) {
        form_error($form['get_contact_bounces']['get_contact_bounces_start'],
          t('Invalid date format given for !name.', array('!name' => $form['get_contact_bounces']['get_contact_bounces_start']['#title'])));
      }
      if (!empty($form_state['values']['get_contact_bounces_end'])
          && !date_create($form_state['values']['get_contact_bounces_end'])) {
        form_error($form['get_contact_bounces']['get_contact_bounces_end'],
          t('Invalid date format given for !name.', array('!name' => $form['get_contact_bounces']['get_contact_bounces_end']['#title'])));
      }
      break;
  }
}

/**
 * Submit handler for test form.
 */
function mailplus_test_form_submit(&$form, &$form_state) {

  $result = NULL;
  mailplus_init_client();
  switch ($form_state['clicked_button']['#value']) {
    case t('Get Contact'):
      $result = MailplusMarcomRest::getContact(
        $form_state['values']['get_contact_id'] === 'all' ? '' : $form_state['values']['get_contact_id']);
      break;

    case t('Get Contact Updates'):
      $start = date_format(date_create($form_state['values']['get_contact_updates_start']), 'c');
      $end = empty($form_state['values']['get_contact_updates_end']) ? REQUEST_TIME :
        date_format(date_create($form_state['values']['get_contact_updates_end']), 'c');
      $result = MailplusMarcomRest::getContactUpdates($start, $end);
      break;

    case t('Get Contact Bounces'):
      $start = date_format(date_create($form_state['values']['get_contact_bounces_start']), 'c');
      $end = empty($form_state['values']['get_contact_bounces_end']) ? REQUEST_TIME :
        date_format(date_create($form_state['values']['get_contact_bounces_end']), 'c');
      $result = MailplusMarcomRest::getContactBounces($start, $end);
      break;

    case t('Get Contact Properties'):
      $result = MailplusMarcomRest::getContactProperties();
      break;

    case t('Get Campaigns'):
      $result = MailplusMarcomRest::getCampaigns();
      break;
  }

  if (isset($result)) {
    // Do not redirect; print the value directly into the form.
    $form_state['redirect'] = FALSE;
    $form['result'] = array(
      '#type' => 'fieldset',
      '#title' => t('RESULTS:'),
      '#weight' => -99,
    );
    // print_r is better on the eye than var_dump. However, it prints 1/''
    // for TRUE/FALSE, which can cause confusion.
    array_walk_recursive($result, function (&$value) {
      if (is_bool($value)) {
        $value = $value ? 'True' : 'False';
      }
    });
    // Since there will be no rebuild (we will just exit the form builder after
    // this submit callback, and the renderer will work on it), use #value.
    $form['result']['output'] = array(
      '#type'          => 'textarea',
      '#rows'          => 20,
      '#weight' => -99,
      '#value' => print_r($result, TRUE),
    );
  }
}
