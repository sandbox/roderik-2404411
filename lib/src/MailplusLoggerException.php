<?php
/**
 * Contains MailplusLoggerException.
 */

namespace Mailplus;

/**
 * Exception thrown by MailplusNullLogger.
 */
class MailplusLoggerException extends \Exception {

  /**
   * Error level: one of the strings defined as log level constants by PSR-3.
   * (e.g. 'emergency', 'alert', 'critical' or 'error'. It's uncommon for an
   * exception to be thrown for levels lower than that.)
   *
   * @var string
   */
  public $level;

  /**
   * Original message which was passed to the logger; may contain placeholders
   * (and differs from $this->getMessage() only in that case).
   *
   * @var string
   */
  public $origMessage;

  /**
   * Extra context which was passed to the logger. This contains placeholders,
   * and 'call_info'.
   *
   * @var array
   *
   * @see MailplusRestClient::getLastCallInfo()
   */
  public $context;

}
