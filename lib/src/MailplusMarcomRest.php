<?php
/**
 * @file
 * Contains MailplusMarcomRest.
 */

namespace Mailplus;

/**
 * Class MailplusMarcomRest
 *
 * Contains helper methods to communicate with Mailplus Marcom via its REST
 * API.
 * The actual communication with the REST API and error handling are separated
 * into another class. We assume that the REST client will request JSON and pass
 * an array back. (In some cases, like dates, the actual value differs between
 * JSON and XML. So the JSON values are documented below.)
 *
 * There are a few methods to interface with that separate class; the other
 * methods map one-to-one to REST API methods documented by Mailplus.
 * For more info: http://kennis.mailplus.nl/wp-content/uploads/2014/11/REST_API_voor_MailPlus_Marcom.pdf
 */
class MailplusMarcomRest
{
  /**
   * The client instance used to do REST API calls.
   *
   * @var MailplusRestClientInterface
   */
  private static $client;

  /**
   * The configuration options used for the client instance. These are used only
   * for checking if a new client should be initialized.
   *
   * @var array
   */
  private static $clientConfig;

  /**
   * Creates a contact (POST /contact)
   *
   * @param array $contact
   *   The 'contact' part of the data array conforming to MailPlus' spec.
   *   Example:
   *   array(
   *     'externalId' => '<OUR OWN GENERATED ID>',
   *     'testGroup' => true,
   *     'properties' => array(
   *       'email' => 'user@example.com',
   *       'firstName' => 'Jane',
   *       'lastName'  => 'Doe',
   *       // ... more standard and custom properties/fields listed here.
   *       // Custom string fields are often called 'profileFieldX', while the
   *       // standard "Interesse" set field is often called 'profileFields'.
   *       'profileField1' => 'Value1',
   *       'profileField2' => 'Value2',
   *       'profileFields' => array(
   *         array('bit' => 1, 'enabled' => true),
   *         array('bit' => 2, 'enabled' => true),
   *       ),
   *     ),
   *   )
   *   If the 'profileFields' field is not specified on create, all its bits are
   *   set to true. If a 'set' field is specified without any bits set (as empty
   *   array; '[]' in the JSON), all bits are set to false. This is intended
   *   behavior for the "Interesse" field (as confirmed by support@mailplus.nl).
   *   If a record is updated instead of created, no bits will be changed in
   *   either case.
   * @param array $options
   *   Options for the contact creation. Valid key/value pairs:
   *   - purge (boolean):
   *     If true, all contact fields not specified will be emptied out. If false
   *     / not specified, they will be left alone. Only useful for update=true.
   *   - update (boolean):
   *     If false / not specified, then a CONTACT_ALREADY_EXISTS error will be
   *     returned if the contact already exists. Which means here: if a contact
   *     with the same externalID OR email already exists.
   *     If 'update' is true, then the following 'deduplication' behavior will
   *     be followed: (NOTE- MailPlus documentation seems to suggest this
   *     behavior is adjustable per MailPlus account...)
   *     - update an existing contact with a matching externalID if possible.
   *       (Also if this record has a different email and another record
   *       exists with the same email; this means there will be two records with
   *       the same email.)
   *       If two contacts with the same externalID exist, one is taken
   *       'randomly' without checking the email field.
   *     - update an existing contact with a matching email if no record with
   *       matching externalID is found. (The old externalID is overwritten.)
   *       If no externalID is specified, the existing externalID is emptied
   *       even if 'purge' is false. If several contacts with the same email
   *       exist, one of them is updated 'randomly'.
   *     - create a new contact otherwise.
   *     'randomly' means we don't know which record is taken. But the same
   *     record seems to be selected consistently. (Highest internal ID?)
   *
   * @return array
   *    Empty array on success, 'error' array on failure.
   */
  public static function createContact(array $contact, array $options = array())
  {
    return self::request('contact', 'POST', array('contact' => $contact) + $options);
  }

  /**
   * Updates a contact (PUT /contact/{externalId})
   *
   * @param array $contact
   *   The 'contact' part of the data array conforming to MailPlus' spec. Its
   *   'externalId' value must be set.
   * @param array $options
   *   Options for the contact update. Valid key/value pairs:
   *   - purge (boolean):
   *     If true, all contact fields not specified will be emptied out. If false
   *     / not specified, they will be left alone.
   *   (update (boolean) has no real meaning here, only at createContact();
   *   providing 'update' = FALSE will yield a BACKEND_EXCEPTION error.)
   *
   * @return array
   *    Empty array on success, 'error' array on failure.
   *
   * @see createContact()
   */
  public static function updateContact(array $contact, array $options = array())
  {
    // It is impossible to 'renumber' the external ID; passing an externalId
    // in the data structure that is different from the URL part will generate
    // an error. So externalId doesn't make sense as a separate method argument,
    // and we require it being set in $contact.
    if (empty($contact['externalId'])) {
      $result = array(
        'errorType' => 'LOCAL',
        'message' => 'externalId not set in updateContact().'
      );
      // Mimic message format used in MailplusRestClient::handleRequestResult().
      self::client()->getLogger()->error(
        '{type} ({status_code}): ' . $result['message'],
        array('type' => 'LOCAL', 'status_code' => '-')
      );
      return $result;
    }

    return self::request(
      'contact/' . rawurlencode($contact['externalId']),
      'PUT',
      array('contact' => $contact) + $options
    );
  }

  /**
   * Gets data for a contact (GET /contact/{externalId})
   *
   * @param string
   *   externalId value of the Mailplus contact. Empty string will return all
   *   contacts.
   *
   * @return array
   *   Contact data, or error response which always includes 'errorType' value.
   *   Example of the former:
   *   array(
   *     'externalId' => '<external id which we specified on creation>',
   *     'created' => '1416862766000', // Timestamp in milliseconds
   *     'encryptedId' => '*Some*Key*',
   *     'testGroup' => '1',
   *     'encryptedId' => '1416862766000',
   *     'lastChanged' => '1416862766000',
   *     'temporary' => '',
   *     'properties' => array(
   *       'email' => 'jane.doe@example.com',
   *       'firstName' => 'Jane',
   *       'phoneNumber' => '',
   *       // ...(not all fields from getContactProperties() are always present)
   *       'profileFields' => array(
   *         array(
   *           'description' => 'interesse',
   *           'bit' => '1',
   *           'enabled' => '1',
   *         ),
   *         array(
   *           'description' => 'interesse2',
   *           'bit' => '2',
   *           'enabled' => '',
   *         ), // ...
   *       ),
   *     ),
   *     'channels' => array(
   *       array(
   *         'name' => 'DM',
   *         'value' => '',
   *       ),
   *       array(
   *         'name' => 'EMAIL',
   *         'value' => '1',
   *       ),
   *       array(
   *         'name' => 'SMS',
   *         'value' => '',
   *       ),
   *     ),
   *   )
   */
  public static function getContact($external_id)
  {
    return self::request('contact/' . rawurlencode($external_id));
  }


  /**
   * Gets updated contacts (GET /contact/updates/list)
   *
   * @param int|string $from
   *   From date; either a timestamp, or ISO format (with or without +NN:00 for
   *   timezone).
   * @param int|string $to
   *   To date; either a timestamp, or ISO format (with or without +NN:00 for
   *   timezone). (Default: now.)
   *
   * @return array
   *   An array of contact objects
   *
   * @see getContact()
   */
  public static function getContactUpdates($from, $to = null)
  {
    if (is_numeric($from)) {
      // This is equivalent to date('c', $from) except it does not emit warnings
      // if the current timezone is not set in php.ini.
      $from = date_format(date_create('@' . $from), 'c');
    }

    if (!isset($to)) {
      $to = date_format(date_create(), 'c');
    }
    elseif (is_numeric($to)) {
      $to = date_format(date_create('@' . $to), 'c');
    }

    return self::request('contact/updates/list', 'GET', array('fromDate' => $from, 'toDate' => $to));
  }

  /**
   * Gets bounced contacts (GET /contact/updates/list)
   *
   * @param int|string $from
   *   From date; either a timestamp, or ISO format (with or without +NN:00 for
   *   timezone).
   * @param int|string $to
   *   To date; either a timestamp, or ISO format (with or without +NN:00 for
   *   timezone). (Default: now.)
   *
   * @return array
   *   An array of contact bounces. A bounce exists of
   *   - contact object
   *   - date when bounce occured
   *   - type: HARDBOUNCE or SOFTBOUNCE
   *   - encryptedActId: the 'encrypted activity Id' for which the bounce
   *     occured (reserved for future extension)
   *   See documentation for an example.
   */
  public static function getContactBounces($from, $to = null)
  {
    if (is_numeric($from)) {
      // This is equivalent to date('c', $from) except it does not emit warnings
      // if the current timezone is not set in php.ini.
      $from = date_format(date_create('@' . $from), 'c');
    }

    if (!isset($to)) {
      $to = date_format(date_create(), 'c');
    }
    elseif (is_numeric($to)) {
      $to = date_format(date_create('@' . $to), 'c');
    }

    return self::request('contact/bounces/list', 'GET', array('fromDate' => $from, 'toDate' => $to));
  }

  /**
   * Lists contact properties (GET /contact/properties/list)
   *
   * @return array
   *   All properties in the format as specified by the documentation. Example
   *   fragment:
   *   array(
   *     array(
   *       'name' => 'email',
   *       'description' => 'E-mailadres',
   *       'type' => 'email',
   *     ),
   *     array(
   *       'name' => 'firstName',
   *       'description' => 'Voornaam',
   *       'type' => 'string',
   *     ),
   *     array(
   *       'name' => 'postalCode',
   *       'description' => 'Postcode',
   *       'type' => 'postcode',
   *     ),
   *     array(
   *       'name' => 'phoneNumber',
   *       'description' => 'Telefoon nr',
   *       'type' => 'string',
   *     ),
   *     array(
   *       'name' => 'mobileNumber',
   *       'description' => 'Mobiel nr.',
   *       'type' => 'mobNr',
   *     ),
   *     array(
   *       'name' => 'profileField1',
   *       'description' => 'Custom label',
   *       'type' => 'string',
   *     ),
   *     array(
   *       'name' => 'profileField2',
   *       'description' => 'Another custom label',
   *       'type' => 'string',
   *     ), // ...
   *     array(
   *       'name' => 'profileFields',
   *       'description' => 'Interesses',
   *       'type' => 'set',
   *       'entries => array(
   *         array(
   *           'bit' => 1,
   *           'description' => 'interesse',
   *           'rank' => 1,
   *         ),
   *         array(
   *           'bit' => 2,
   *           'description' => 'inter2',
   *           'rank' => 2,
   *         ), // ...
   *       ),
   *     ),
   *   )
   */
  public static function getContactProperties()
  {
    return self::request('contact/properties/list');
  }

  /**
   * Lists campaigns (GET /campaign/list)
   *
   * @return array
   *   All active campaigns. Example:
   *   array(
   *     array(
   *       'encryptedId' => '*Some*Key*',
   *       'description' => 'Aanmeldbevestiging', // subscription confirmation
   *       'active' => '',
   *     ),
   *   )
   */
  public static function getCampaigns()
  {
    return self::request('campaign/list');
  }

  // @todo Implement triggerCampaign(), stopCampaign().

  /**
   * Performs a HTTP request to a REST API endpoint.
   *
   * This method does nothing special but leading all calls through here makes
   * some logic more easily extensible.
   *
   * @see MailplusRestClientInterface::handleRequest()
   */
  protected static function request($endpoint, $http_method = 'GET', array $data = array())
  {
    return self::client()->handleRequest($endpoint, $http_method, $data);
  }

  /**
   * Returns a client instance used to do REST API calls; if none was set yet,
   * initializes a default client class when necessary.
   *
   * @param array $config
   *   (optional) Configuration values for the client. These must be passed on
   *   the first call (or setClient() must be called before calling this method)
   *   in order to retrieve a client that actually works. It's not mandatory to
   *   pass them on subsequent calls.
   *   Passing an empty array will prohibit this method from initializing a new
   *   instance and will cause it to return null if no client is initialized
   *   yet; this can be used to check whether a client already exists.
   *   Passing no argument will cause this method to always return an instance,
   *   even if none is initialized yet; in that case handleRequest() can be
   *   called on it but will likely trigger an error during execution
   *   (because of missing credentials).
   *
   * @return MailplusRestClientInterface|null
   *   null if an empty array is passed as $config argument and no instance was
   *   initialized yet; otherwise, an already-initialized instance if one exists
   *   and $config is not passed or equal to the current instance's config;
   *   otherwise, a newly initialized instance of the default client class.
   */
  public static function client(array $config = null)
  {
    if ($config !== array()) {

      if (isset($config) && $config != self::$clientConfig) {
        // Reinstantiate client with different config.
        self::$client = null;
      }

      if (!isset(self::$client)) {
        self::$clientConfig = isset($config) ? $config : array();
        self::$client = new MailplusRestClient(self::$clientConfig);
      }
    }

    return self::$client;
  }

  /**
   * Sets the client instance used to do REST API calls. This method should be
   * called before any other method, if you want to work with a non-default
   * client class.
   *
   * @param MailplusRestClientInterface $client
   *   (optional) a MailplusRestClient instance. If no argument is passed, any
   *   current client instance will be destroyed so a next call to client()
   *   will always instantiate a new client.
   *
   * @return MailplusRestClientInterface
   *   The client object that was passed.
   */
  public static function setClient(MailplusRestClientInterface $client = null)
  {
    self::$clientConfig = array();
    return self::$client = $client;
  }
}
