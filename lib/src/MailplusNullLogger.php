<?php
/**
 * Contains MailplusNullLogger.
 */

namespace Mailplus;

/**
 * Class MailplusNullLogger
 *
 * Implements PSR-3 LoggerInterface (though not officially, because the sources
 * in this directory have no external dependency); is a copy of
 * https://github.com/php-fig/log/blob/master/Psr/Log/NullLogger.php but with
 * code put together in one class, and log level constants dereferenced to the
 * standard strings.
 *
 * This logger doesn't log things, but above a configurable 'threshold' severity
 * an exception is optionally thrown. This can be done e.g. to prevent having to
 * check every return value for 'errorType' (when this class is used by
 * MailplusRestClient).
 *
 * This class only exists so we have a default instance to use in
 * MailplusRestClient; you are expected to always override in your own code
 * (unless you can live without logging, and with just throwing exceptions).
 *
 * One incompatibility with the PSR-3 specification: unknown log levels cause a
 * MailplusLoggerException to be thrown, not a Psr\Log\InvalidArgumentException.
 */
class MailplusNullLogger
{

  /**
   * Map of PSR-3 logging constants to numeric levels; lower is more severe.
   */
  protected static $levelMap = array(
    'emergency' => 0,
    'alert' => 1,
    'critical' => 2,
    'error' => 3,
    'warning' => 4,
    'notice' => 5,
    'info' => 6,
    'debug' => 7,
  );

  /**
   * Class configuration which only needs to be set by constructor. It is not
   * expected that code will need to check this information (there is no getter)
   * because the code should function independently of how the user / specific
   * website configures the logger.
   *
   * @see __construct()
   *
   * @var array
   */
  protected $config;

  /**
   * Constructor function.
   *
   * @param array $config
   *   (optional) Configuration options for this class. Only the 'exception'
   *   key has any meaning here. The value is a log level. If events equal to or
   *   more severe than this level are logged, an exception will be thrown.
   *   This argument is an array to make the class more easily extensible.
   */
  function __construct(array $config = array()) {
    $this->config = $config;
  }

  /**
   * System is unusable.
   *
   * @param string $message
   * @param array $context
   * @return null
   */
  public function emergency($message, array $context = array())
  {
    $this->log('emergency', $message, $context);
  }

  /**
   * Action must be taken immediately.
   *
   * Example: Entire website down, database unavailable, etc. This should
   * trigger the SMS alerts and wake you up.
   *
   * @param string $message
   * @param array $context
   * @return null
   */
  public function alert($message, array $context = array())
  {
    $this->log('alert', $message, $context);
  }

  /**
   * Critical conditions.
   *
   * Example: Application component unavailable, unexpected exception.
   *
   * @param string $message
   * @param array $context
   * @return null
   */
  public function critical($message, array $context = array())
  {
    $this->log('critical', $message, $context);
  }

  /**
   * Runtime errors that do not require immediate action but should typically
   * be logged and monitored.
   *
   * @param string $message
   * @param array $context
   * @return null
   */
  public function error($message, array $context = array())
  {
    $this->log('error', $message, $context);
  }

  /**
   * Exceptional occurrences that are not errors.*
   * Example: Use of deprecated APIs, poor use of an API, undesirable things
   * that are not necessarily wrong.
   *
   * @param string $message
   * @param array $context
   * @return null
   */
  public function warning($message, array $context = array())
  {
    $this->log('warning', $message, $context);
  }

  /**
   * Normal but significant events.
   *
   * @param string $message
   * @param array $context
   * @return null
   */
  public function notice($message, array $context = array())
  {
    $this->log('notice', $message, $context);
  }

  /**
   * Interesting events.
   *
   * Example: User logs in, SQL logs.
   *
   * @param string $message
   * @param array $context
   * @return null
   */
  public function info($message, array $context = array())
  {
    $this->log('info', $message, $context);
  }

  /**
   * Detailed debug information.
   *
   * @param string $message
   * @param array $context
   * @return null
   */
  public function debug($message, array $context = array())
  {
    $this->log('debug', $message, $context);
  }

  /**
   * Logs with an arbitrary level.
   *
   * @param mixed $level
   * @param string $message
   * @param array $context
   * @return null
   * @throws MailplusLoggerException
   */
  public function log($level, $message, array $context = array())
  {

    if (!isset(static::$levelMap[$level])) {
      // @todo Throw exception; PSR-3 requires that.
    }

    // Unless it's specified that we should throw an exception, we do nothing.
    // Throw exception if configured. Ignore unknown log levels.
    if (isset($this->config['exception'])
        && isset(static::$levelMap[$this->config['exception']])
        && static::$levelMap[$level] <= static::$levelMap[$this->config['exception']]) {

//@todo test this. We shouldn't be replacing strings at all, but should be
// passing them into the exception and overriding getMessage(). Test again whether we can
// override setMessage(). (I think we cannot override the constructor, right?)

        // Expand placeholders in message, because we can't expect the caller to
        // find the placeholders. (There is no standard for this.) Since there are no standards on how
        // code should handle placeholder text
        // Values are considered 'not sanitized'; there really is no make it suitable for direct
        // printing in (X/HT)ML. Replace the 'minimum' amount of characters,
        // plus quotes. (The encoding of the single quote makes this invalid
        // HTML4, but valid in all other *ML.)
//        $msg = $message;
//        foreach ($context as $key => $value) {
//          $msg = str_replace('{' . $key . '}', $value, $msg);
//        }

      // Check for exception thrown.
      $code = 0;
      $previous = null;
      if (isset($context['exception']) && $context['exception'] instanceof \Exception) {
        $previous = $context['exception'];
        unset($context['exception']);
        $code = $previous->getCode();
      }

      $exception = new MailplusLoggerException($msg, $code, $previous);
//@todo after we have done the above, can origMessage be ditched?
      $exception->origMessage = $message;
      $exception->level = $level;
      $exception->context = $context;
      throw $exception;
    }
  }
}
