<?php
/**
 * @file
 * Contains MailplusRestClient.
 */

namespace Mailplus;

/**
 * Class MailplusRestClient
 *
 * Contains code to communicate with a Mailplus REST API over HTTP and
 * authenticate using OAuth.
 * This is split out into a separate class so it can be overridden separately
 * from the API level functions. You might want to override it if you want to
 * use different libraries for HTTP communication or OAuth header generation.
 *
 * This class uses curl, and no OAuth libraries.
 */
class MailplusRestClient implements MailplusRestClientInterface
{

  // REST endpoint, including trailing slash.
  const BASE_URL = 'https://restapi.mailplus.nl/integrationservice-1.1.0/';

  /**
   * OAuth Consumer key.
   *
   * @var string
   */
  protected $oauthConsumerKey = '';

  /**
   * OAuth Consumer secret.
   *
   * @var string
   */
  protected $oauthConsumerSecret = '';

  /**
   * PSR-3 compatible logger class instance.
   *
   * Some words about what info this class will usually log in what way, which
   * may or may not be obvious (it follows from the structure of PHP/PSR-3):
   * The log message will not contain file/function names and line numbers,
   * since that can be derived from the exception (if it's passed) or
   * debug_backtrace() info.
   * The errorType/method/url will often be included in the log message because
   * there is no standardized place for a logger to look for it (unlike
   * 'exception') so we have no guarantee that the logger will log it if it
   * isn't in the message. (We'll use placeholders.)
   *
   * @var LoggerInterface|MailplusNullLogger
   */
  protected $logger;

  /**
   * Info about the last REST API call made.
   *
   * @var array
   */
  protected $lastCallInfo;

  /**
   * {@inheritdoc}
   *   If key & secret are not passed, the client will very likely not work.
   *   Keys/values used by this class:
   *   - key: REST API key
   *   - secret: REST API secret
   *   - logger: (optional) PSR-3 compatible logger class instance. This class
   *     class will pass $this->lastCallInfo as 'call_info' in its context, so
   *     its nice if the logger supports logging this info where needed. If not
   *     passed, a 'null logger' will be used, and only the last log will be
   *     available through $this->getLastCallInfo().
   */
  public function __construct(array $config)
  {
    $this->oauthConsumerKey = isset($config['key']) ? $config['key'] : '';
    $this->oauthConsumerSecret = isset($config['secret']) ? $config['secret'] : '';
    if (isset($config['logger'])) {
      $this->logger = $config['logger'];
    } else {
      $this->logger = new MailplusNullLogger();
    }
  }

  /**
   * {@inheritdoc}
   */
  public function handleRequest($url, $http_method = 'GET', array $data = array())
  {
    $url = static::BASE_URL . $url;

    $this->lastCallInfo = array(
      'url' => $this->normalizeUrl($url),
      'method' => strtoupper($http_method),
      'parameters' => $data,
      'time' => time(),
    );

    if (is_string($http_method)) {
      if (in_array($this->lastCallInfo['method'], array('GET', 'POST', 'PUT'))) {

        // Trim all values, to prevent hard to trace bugs caused by leading/trailing
        // spaces.
        array_walk_recursive($data, function (&$value) {
          if (is_string($value)) {
            $value = trim($value);
          }
        });

        $params_string = '';
        $url_params = array();
        if ($data && $this->lastCallInfo['method'] === 'GET') {
          $url_params = $data;
          $values = array();
          foreach ($data as $key => $value) {
            $values[] = rawurlencode($key) . '=' . rawurlencode($value);
          }
          $params_string = '?' . implode('&', $values);
        }

        $http_header = array(
          $this->oauthHeaderLine($this->lastCallInfo['method'], $this->lastCallInfo['url'], $url_params),
          'Accept: application/json',
        );
        if ($this->lastCallInfo['method'] !== 'GET') {
          $http_header[] = 'Content-Type: application/json';
        }

        $curl = curl_init();
        curl_setopt_array($curl, array(
          CURLOPT_URL => $this->lastCallInfo['url'] . $params_string,
          CURLOPT_HTTPHEADER => $http_header,
          // Return the response body from curl_exec() instead of the HTTP
          // status code. The body always neatly follows the status code
          // and contains 'errorType'. If we want to remember the status code
          // we do it below.
          CURLOPT_RETURNTRANSFER => true,
          CURLOPT_SSL_VERIFYPEER => false,
        ));
        if (strtoupper($http_method) !== 'GET') {
          // POSTFIELDS is actually just a string to post as the body.
          // (It has some more applications, but that's not important here.)
          curl_setopt($curl, CURLOPT_POSTFIELDS, json_encode($data));
          if (strtoupper($http_method) === 'PUT') {
            // Not CURLOPT_PUT = true (that means something different), but:
            curl_setopt($curl, CURLOPT_CUSTOMREQUEST, 'PUT');
          }
        }
        $response = curl_exec($curl);
        $result = empty($response) ? array() : json_decode($response, true);
        if (isset($result['errorType'])) {
          // Also get response code; maybe some caller wants to check it.
          $result['statusCode'] = curl_getinfo($curl, CURLINFO_HTTP_CODE);
        }
        curl_close($curl);

      } else {
        $result = array(
          'errorType' => 'LOCAL',
          'message' => 'HTTP method not supported for {method} {url}.',
          'messagePlaceholders' => array(
            'method' => $http_method,
            'url' => $url,
          )
        );
      }
    } else {
      $result = array(
        'errorType' => 'LOCAL',
        'message' => 'HTTP method called on url {url} is not a string: {method}.',
        'messagePlaceholders' => array(
          'method' => print_r($http_method, true),
          'url' => $url,
        )
      );
    }

    return $this->handleRequestResult($result);
  }

  /**
   * Normalize URL. Perhaps needed to build proper OAuth header.
   */
  protected function normalizeUrl($url) {
    $parts = parse_url($url);

    $scheme = isset($parts['scheme']) ? $parts['scheme'] : 'http';
    $port = isset($parts['port']) ? $parts['port'] : ($scheme == 'https' ? '443' : '80');
    $host = isset($parts['host']) ? strtolower($parts['host']) : '';
    $path = isset($parts['path']) ? $parts['path'] : '';

    if ($scheme == 'https' && $port != '443'
        || $scheme == 'http' && $port != '80') {
      $host = "$host:$port";
    }
    return "$scheme://$host$path";
  }

  /**
   * Constructs OAuth header line.
   *
   * @return string
   */
  protected function oauthHeaderLine($method, $url, $params = array())
  {
    // Thanks to http://hannah.wf/twitter-oauth-simple-curl-requests-for-your-own-data/
    // and oauth-googlecode/OAuth.php (which we didn't want to include just for
    // this - plus the below is easier to follow in our case (no tokens).)

    $oauth_params = array(
      'oauth_version' => '1.0',
      'oauth_nonce' => md5(microtime() . mt_rand()),
      'oauth_timestamp' => time(),
      'oauth_consumer_key' => $this->oauthConsumerKey,
      'oauth_signature_method' => 'HMAC-SHA1',
    );

    // Build base string.
    // rawurlencode is suitable for this purpose from PHP >= 5.3.
    $params += $oauth_params;
    ksort($params);
    $values = array();
    foreach ($params as $key => $value) {
      $values[] = rawurlencode($key) . '=' . rawurlencode($value);
    }
    $base_info = $method . '&' . rawurlencode($url) . '&' . rawurlencode(implode('&', $values));

    // Add HMAC-SHA1 signature, from base string/key, to OAuth params.
    $composite_key = rawurlencode($this->oauthConsumerSecret) . '&'; // no token secret here: still end with '&'
    $oauth_params['oauth_signature'] = base64_encode(hash_hmac('sha1', $base_info, $composite_key, true));

    // Build header line from params.
    $values = array();
    foreach($oauth_params as $key => $value) {
      $values[] = "$key=\"" . rawurlencode($value) . "\"";
    }
    return 'Authorization: OAuth ' . implode(',', $values);
  }

  /**
   * Perform any necessary actions on the result from the HTTP request:
   * add to / normalize lastCallInfo, and log things (errors).
   *
   * @param array $result
   *   The decoded (into an array) result body, or an array containing
   *   'errorType' created by the caller. This can contain anything documented
   *   at MailplusRestClientInterface::getLastCallInfo(). Notes:
   *   - if 'messagePlaceholders' is an array, the message is assumed to be safe
   *     and will not be encoded/sanitized. This means if you want to pass a
   *     message containing HTML that should not be encoded, but does not
   *     contain {placeholder}s, pass an empty array in 'messagePlaceholders'.
   *     (And make sure the whole message is indeed safe!)
   *   - 'messageTemplate' should not be used because it will be overwritten
   *     (with the original text passed in 'message'.)
   *
   * @return array
   *   The result body as passed in.
   */
  protected function handleRequestResult($result) {
    // This is split out into its own method so it's a bit easier to extend, and
    // callers can 'return handleRequestResult($result);' to exit from anywhere.

    if (isset($result['errorType'])) {

      // Check for bogus argument, to make the below code easier:
      if (isset($result['messagePlaceholders']) && !is_array($result['messagePlaceholders'])) {
        $this->logger->notice('non-array messagePlaceholders encountered.');
        unset($result['messagePlaceholders']);
      }

      // lastCallInfo already has the 'url' info set; we need to add errors.
      // For server errors, the spec says errorType and message are set, always.
      $this->lastCallInfo = array_merge($this->lastCallInfo, $result);

      // Do not log 404s from a GET request; only set the lastCallInfo.
      // (NOT_AUTHORIZED errors return a 403, not 401. We want to log those.)
      if (!isset($result['statusCode']) || $result['statusCode'] != 404 || $this->lastCallInfo['method'] !== 'GET') {

        // We will provide extra 'call_info' to the logger, though we can't be
        // sure it will actually get logged. (This kind of extension is in line
        // with PSR-3.) Since we pass the message in separate arguments, the
        // 'call_info' does not need to contain it. debugInfo is passed, even
        // though we'll make a separate call for it later. (In case this is
        // needed by a logger class which needs all info at the same time.)
        $context = array(
          'type' => $result['errorType'],
          'status_code' => isset($result['statusCode']) ? $result['statusCode'] : '-',
          'call_info' => array_diff_key($this->lastCallInfo,
            array(
              'errorType' => true,
              'statusCode' => true,
              'message' => true,
              'origMessage' => true,
              'messagePlaceholders' => true,
              'exception' => true,
            ))
        );
        if (!empty($result['exception'])) {
          // As mandated by PSR-3:
          $context['exception'] = $result['exception'];
        }

        // (Security) consideration for logging:
        // an error message itself can be either considered 'unsafe' (in which
        // case it must be logged as a placeholder in another message, which
        // means it's not translatable), or translatable (which means it can
        // also contain placeholders, meaning the message itself must be safe).
        // Other considerations:
        // Some of MailPlus' messages are short (e.g. "Contact does not exist")
        // and so they could benefit from translation; others already have
        // placeholders expanded (e.g. "MailPlus IntegrationService
        // BackendException occured at Thu Nov 27 18:19:39 CET 2014") and should
        // not be run through a translation layer.
        // Conclusion:
        // We'll treat remote messages as unsafe (and untranslatable). It's
        // secure. Only if 'messagePlaceholders' is defined (for local errors),
        // we will consider them safe.
        if (isset($result['messagePlaceholders'])) {
          // We can assume 'call_info', 'exception' and 'type' are not used as
          // placeholders, since we defined all messages/placeholders ourselves.
          if ($result['messagePlaceholders']) {
            $context += $result['messagePlaceholders'];
          }
          $this->logger->error('{type} ({status_code}): ' . $result['message'], $context);
        } else {
          $this->logger->error('{type} ({status_code}): {message}', $context + array(
              'message' => $result['message'],
            ));
        }

        // Log debug info separately, because we can't assume a PSR-3 compatible
        // logger did something with the 'debugInfo' value in the previous call.
        // (A logger that actually did something with that debugInfo previously,
        // could recognize this call by its context, and ignore it.)
        // (A note: this was made to accommodate for code using the PECL OAuth
        // class, which throws exceptions (with a debugInfo property) for e.g.
        // HTTP 40x responses. We don't use the OAuth class by default anymore
        // but will keep supporting debugInfo here.)
        if (!empty($result['debugInfo'])) {
          // print_r is better on the eye than var_dump. However, it prints
          // 1/'' for TRUE/FALSE which can cause confusion.
          array_walk_recursive($result['debugInfo'], function (&$value) {
            if (is_bool($value)) {
              $value = $value ? 'True' : 'False';
            }
          });
          $this->logger->debug('Extra info accompanying previous error: {mailplus_extra_info}', array(
            'mailplus_extra_info' => "Debug info:\n" . print_r($result['debugInfo'], true),
            'call_info' => array_diff_key($this->lastCallInfo,
              // According to PSR-3, an exception must not be anywhere else than
              // directly in $context['exception'] - and we don't want to pass
              // the same exception twice.
              array('debugInfo' => true, 'exception' => true)),
          ));
        }
      }

      // Normalize 'message' to have its placeholders replaced.
      $result['messageTemplate'] = $result['message'];
      if (!isset($result['messagePlaceholders'])) {
        // Message is considered 'not sanitized'; make it suitable for direct
        // printing in (X/HT)ML. Replace the 'minimum' amount of characters,
        // plus quotes. (The encoding of the single quote makes this invalid
        // HTML4, but valid in all other *ML. The below is the PHP<5.4
        //  equivalent to "htmlspecialchars($text, ENT_QUOTES | ENT_HTML5)".)
        $result['message'] =  str_replace("'", '&apos;', htmlspecialchars($result['message']));
      } elseif ($result['messagePlaceholders']) {
        // Replace / sanitize placeholders.
        foreach ($result['messagePlaceholders'] as $key => $value) {
          $result['message'] = str_replace(
            '{' . $key . '}',
            str_replace("'", '&apos;', htmlspecialchars($value)),
            $result['message']
          );
        }
      }
    }

    return $result;
  }

  /**
   * {@inheritdoc}
   */
  public function getLastCallInfo($type = '')
  {
    $info = isset($this->lastCallInfo) ? $this->lastCallInfo : array();
    if (!empty($type)) {
      return isset($info[$type]) ? $info[$type] : '';
    }
    return $info;
  }

  /**
   * {@inheritdoc}
   */
  public function getLogger()
  {
    return $this->logger;
  }

}
