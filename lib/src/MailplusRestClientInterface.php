<?php
/**
 * @file
 * Contains MailplusRestClientInterface.
 */

namespace Mailplus;

/**
 * Interface for a MailPlus Rest Client.
 */
interface MailplusRestClientInterface
{

  /**
   * Constructor function.
   *
   * @param array $config
   *   Configuration values.
   */
  public function __construct(array $config);

  /**
   * Performs a HTTP request to a REST API endpoint; optionally does error
   * handling.
   *
   * @param string $url
   *   Relative URL for the API method (not including the API endpoint URL).
   * @param string $http_method
   *   HTTP method: GET, POST or PUT.
   * @param array $data
   *   For GET: arguments. For POST/PUT: data to encode as JSON in the request
   *   body.
   *
   * @return array|null
   *   The JSON response body -or a local error condition- as an array; null if
   *   JSON decoding failed. Errors (local or from the server) always have the
   *   'errorType' key set, unlike non-error responses. If 'errorType' is
   *   present, a 'message' key should be present too (and possibly more info,
   *   all of which and more can also be retrieved from getLastCallInfo()).
   */
  public function handleRequest($url, $http_method = 'GET', array $data = array());

  /**
   * Returns the active (PSR-3 compatible) logger.
   *
   * @return LoggerInterface|MailplusNullLogger
   */
  public function getLogger();

  /**
   * Returns meta info about the last MailPlus call or other errors.
   * This is necessary to get error information after e.g. a GET request which
   * always returns a data array, has returned empty array (which could indicate
   * either 'no data' or 'error', error conditions could not be returned in the
   * data array.)
   * Most POST/PUT requests actually return an array containing keys 'errorType'
   * and 'message' on error; calling this method will in that case return the
   * same values, plus more.
   *
   * Implementing classes make sure that values are present as expected (e.g.
   * that message is always sanitized, and an unsanitized version without
   * placeholders is present in messageTemplate). Note that this means the below
   * types of information do not correspond 100% to the information passed
   * internally inside a class. See the implementing class' code/documentation
   * for details on that.
   *
   * @param string $type
   *   Type of information to return. Can be:
   *   - errorType:   string; empty if no error. Most types are provided by the
   *                    REST server. error types not provided by the server are:
   *                    LOCAL_REQUEST: e.g. Curl errors if the request is made
   *                                   through Curl
   *                    LOCAL_OAUTH: OAuth library exception
   *                    LOCAL: anything else caused by local code/config
   *                    We've seen the following server-side types so far:
   *                    BACKEND_EXCEPTION, CONTACT_ALREADY_EXISTS,
   *                    CONTACT_NOT_FOUND, CONTACT_OPTOUT, INVALID_PROPERTY
   *                    (They're not all documented in MailPlus' manual.)
   *   - message:     string; (error) message, 'safe' / with placeholders
   *                    replaced.
   *   - origMessage: string; original (error) message, unsanitized / still
   *                    containing placeholders marked with {}.
   *   - messagePlaceholders: array; placeholder values for messageTemplate.
   *                    These should be considered 'not sanitized yet'.
   *   - statusCode:  string: HTTP status code from response. Is not always set.
   *   - debugInfo:   array; debug info, possibly including response message
   *                    from HTTP header.
   *   - exception:   object; exception thrown for this error.
   *   - url:         string; full URL called (including service 'base url').
   *   - method:      string; REST method called
   *   - parameters:  array; parameters provided to the API call.
   *   - time:        int; timestamp of the start of the API call.
   * 'safe' means a message can be safely printed in a UTF-8 encoded HTML(>4)
   * document. It could contain HTML if some code used HTML in a message/
   * placeholders, and this code has made sure this does not contain 'dangerous'
   * parts. The caller is expected not to not re-encode 'message'; when needing
   * to do your own sanitizing/encoding/translation, use 'messageTemplate' and
   * 'messagePlaceholders' (if it exists).
   *
   * @return mixed
   *  If $type is empty, all these values are returned in an encompassing array.
   *  In that case, not all elements are guaranteed to be set. Specifically:
   *  if there is no error, only url/method/parameters/time are set; if there
   *  is an error, only errorType/message/messageTemplate are guaranteed to be
   *  set as well.
   *
   * @see handleRequest()
   */
  public function getLastCallInfo($type = '');

}
