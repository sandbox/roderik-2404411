<?php
/**
 * @file
 * Contains MailplusRestClient.
 */

namespace Mailplus;

/**
 * Class MailplusRestOAuthClient
 *
 * Contains a version of MailplusRestClient which uses the PECL OAuth library
 * (which does not necessarily use the curl library) for GET requests.
 * POST/PUT requests still need to be done. Contributions welcome.
 */
class MailplusRestOAuthClient extends MailplusRestClient implements MailplusRestClientInterface
{
  /**
   * {@inheritdoc}
   */
  public function handleRequest($url, $http_method = 'GET', array $data = array())
  {

    if (is_string($http_method) && $this->lastCallInfo['method'] == 'GET') {

      $url = static::BASE_URL . $url;
      $this->lastCallInfo = array(
        'url' => $this->normalizeUrl($url),
        'method' => strtoupper($http_method),
        'parameters' => $data,
        'time' => time(),
      );

      // Trim all values, to prevent hard to trace bugs caused by leading/trailing
      // spaces.
      array_walk_recursive($data, function (&$value) {
        if (is_string($value)) {
          $value = trim($value);
        }
      });

      try {
        // Use PECL OAuth library (which has is documented on php.net but
        // not completely, as of december 2014, and has not passed 1.0 yet).
        $oauth = new \OAuth($this->oauthConsumerKey, $this->oauthConsumerSecret);
        $success = $oauth->fetch($url, $data, OAUTH_HTTP_METHOD_GET,  array('Accept' => 'application/json'));
        if ($success) {
          $result = json_decode($oauth->getLastResponse(), true);
        } else {
          // This should not happen, because non-success statuses throw
          // an exception.
          // We return something just to be sure. If needed, we can include
          // $oauth->getLastResponse() or $oauth->getLastResponseInfo().
          $result = array(
            'errorType' => 'LOCAL_OAUTH',
            'message' => 'MailplusRestClient GET request failed. detailed info is not logged.',
          );
        }
      } catch (\OAuthException $e) {
        // A non-success status throws an exception, but just returns the
        // specified response from the Mailplus server. (Also on e.g. a
        // GET request for a nonexistent contact, we get a JSON response.)
        $result = json_decode($e->lastResponse, true);
        if (isset($result['errorType']) && isset($result['message'])) {
          // Add the info from the exception:
          $result['statusCode'] = $e->getCode();
          if ($e->debugInfo) {
            // This is an array, specific to OAuthException.
            $result['debugInfo'] = $e->debugInfo;
            // Add the Exception message to debugInfo.
            // The message does not seem to be the message from the HTTP
            // response header (or at least something was added to it).
            array_unshift($result['debugInfo'], 'Message: ' . $e->getMessage());
          }
          else {
            $result['debugInfo'] = array($e->getMessage());
          }

        } else {
          // We don't know what kind of exception this is.
          $r = array(
            'errorType' => 'LOCAL_OAUTH',
            'message' => 'OAuth exception thrown: {code}: {message}',
            'messagePlaceholders' => array(
              'code' => $e->getCode(),
              'message' => $e->getMessage(),
            ),
            'exception' => $e,
          );
          if ($e->debugInfo) {
            // As we can see below, the general 'logging strategy' is to
            // - first log an error message, and pass the exception if it's
            //   there, so the logger can do with it as it pleases;
            // - then if there is debug info, log that separately.
            // Funny thing: OAuthException::__toString() (in php <= 5.5.20)
            // does not include its own extra (lastResponse and debugInfo)
            // properties in its output. So we assume the logger does not
            // log those with the error, and add them as separate debugInfo
            // for them to get logged. (Wait, am I sure about debugInfo?
            // or has it just always been empty?)
            $r['debugInfo'] = $e->debugInfo;
            array_unshift($r['debugInfo'], "Response: $result");
          }
          else {
            $r['debugInfo'] = array("Response: $result");
          }
          $result = $r;
        }
      } catch (\Exception $e) {
        $result = array(
          'errorType' => 'LOCAL',
          'message' => 'Exception thrown while executing HTTP GET {url}: {code}: {message}.',
          'messagePlaceholders' => array(
            'code' => $e->getCode(),
            'message' => $e->getMessage(),
            'url' => $url,
          ),
          'exception' => $e,
        );
      }
    } else {
      // @todo: handle POST/PUT with PECL OAuth.
      return parent::handleRequest($url, $http_method, $data);
    }

    return $this->handleRequestResult($result);
  }

}
